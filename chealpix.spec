Name: chealpix
Version: 3.30.0
Release: 1%{?dist}
Summary: HEALPix C interface
License: GPLv2+
URL: http://healpix.sourceforge.net
Group: Development/Libraries
Source: https://downloads.sourceforge.net/healpix/Healpix_3.30/%{name}-%{version}.tar.gz
Packager: Leo Singer <leo.singer@ligo.org>, Edward Maros <ed.maros@ligo.org>
BuildRequires: cfitsio-devel pkgconfig
Requires: cfitsio

%description
HEALPix is an acronym for Hierarchical Equal Area isoLatitude Pixelization
of a sphere. As suggested in the name, this pixelization produces a
subdivision of a spherical surface in which each pixel covers the same
surface area as every other pixel. This package provides a C language
implementation of HEALPix.

%package devel
Summary: Files needed for compiling programs that use %{name}
Group: Development/Libraries
Requires: %{name} = %{version}-%{release}
Requires: cfitsio-devel
%description devel
Headers and libraries needed for compiling programs that use %{name}

%prep
%setup -q

%build
%configure
%{__make}

%install
%makeinstall
rm -f $RPM_BUILD_ROOT%{_libdir}/*.la

%check
make check

%post -p /sbin/ldconfig

%postun -p /sbin/ldconfig

%files
%defattr(-,root,root)
%{_libdir}/*.so.*

%files devel
%defattr(-,root,root)
%{_libdir}/*.so
%{_libdir}/*.a
%{_libdir}/pkgconfig/*
%{_includedir}/*

%changelog
* Thu May 19 2016 Leo Singer <leo.singer@ligo.org> 3.30.0-1

- New upstream release

- Run unit tests

- Fix some rpmlint warnings

* Thu Mar 31 2016 Edward Maros <ed.maros@ligo.org> - 3.10-1

- Added dist to release tag

- Changed packager to Edward Maros
