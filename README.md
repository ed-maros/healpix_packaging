# Project Name

Healpix Packaging

## Installation

There is no installation of this package,
instead, this is a collection of rules
necessary to package portions of healpix
and derived packages for use by LIGO and
the LSC.

## Usage

rpmbuild -ba healpix_cxx.spec
rpmbuild -ba chealpix.spec
rpmbuild -ba healpy.spec

## Contributing

## History

TODO: Write history

## Credits

Edward Maros <ed.maros@ligo.org>
Leo Singer <leo.singer@ligo.org>

## License

GPL V3