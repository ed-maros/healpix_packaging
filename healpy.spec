Summary: Healpix tools package for Python
Name: healpy
Version: 1.6.1
Release: 5%{?dist}
Source: https://pypi.python.org/packages/source/h/%{name}/%{name}-%{version}.tar.gz
License: GPLv2
Group: Development/Libraries
Vendor: C. Rosset, A. Zonca <cyrille.rosset@apc.univ-paris-diderot.fr>
Packager: Leo Singer <leo.singer@ligo.org>, Edward Maros <ed.maros@ligo.org>
Requires: healpix_cxx cfitsio pyfits numpy python-matplotlib
Url: http://github.com/healpy
BuildRequires: python-devel python-setuptools pkgconfig healpix_cxx-devel >= 3.10-1.20130412svn490 cfitsio-devel numpy

%description
Healpy provides a python package to manipulate healpix maps. It is based on the standard numeric and visualisation tools for Python, Numpy and matplotlib.

%prep
%setup -q

%build
env CFLAGS="$RPM_OPT_FLAGS" python setup.py build

%install
python setup.py install --single-version-externally-managed -O1 --root=$RPM_BUILD_ROOT --record=INSTALLED_FILES

%clean
rm -rf $RPM_BUILD_ROOT

%files -f INSTALLED_FILES
%defattr(-,root,root)

%changelog
* Thu Mar 31 2016 Edward Maros <ed.maros@ligo.org> 1.6.1-5
- Added dist to release tag
- Changed packager to Edward Maros
